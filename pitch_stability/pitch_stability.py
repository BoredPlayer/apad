# -*- coding: UTF-8 -*-
import math
import numpy as np
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

inputfile = open("input/input_list.txt", "r")

S = 0.3
l = 1.023
Sh = 0.03307
lh = 0.506
Lmbd = 4/0.3/1.08
k_zakl = -4.83#kąt zaklinowania [rad]
a1 = 3.109

paleta = open("input/paleta.color", 'r')

paleta_line = paleta.readline()
colors = paleta_line.split(" ")
paleta.close()

#sprawdzenie, czy istnieje folder output
if(not os.path.exists('output')):
	try:
		print("Creating \"output\" folder.\n")
		os.mkdir("output")
	except:
		print("Warning! Could not find nor create folder \"output\"!\n")

Cz_tab = []#[alfa, cx, cz]
Czp_t = [[], []]#alfa, cz
#Czytanie plików z Cz i Cx
for line in inputfile:
    ll=line.split("\t")
    for czfilename in ll:
        print("Reading: "+czfilename+"\n")
        czfile = open(czfilename, "r")
        Cz_tab.append([[],[],[]])
        for lf in czfile:
            llf = lf.split("\t")
            if(len(llf[1])>5):
                for i, val in enumerate(llf):
                    Cz_tab[-1][i].append(float(val))
        czfile.close()

inputfile.close()

#Czytanie pozycji silnika
inputfile = open("input/pozycja_silnika.txt", "r")
poz_sil = [[], [], []]#m, x, z
print("Reading: input/aerodynamika_ogona.txt\n")
for line in inputfile:
    ll=line.split("\t")
    for i in range(len(ll)):
        if ',' in ll[i]:
            w_str = list(ll[i])
            w_str[w_str.index(',')] = '.'#zamiana przecinka na kropkę
            ll[i] = "".join(w_str)
    if(len(ll[0])>2):
        poz_sil[0].append(float(ll[0]))
        poz_sil[1].append(float(ll[1]))
        poz_sil[2].append(float(ll[2]))

inputfile.close()

#Czytanie pliku z Cz(alfa) płata
inputfile = open("input/Czpalfa.txt", "r")
print("Reading: input/Czpalfa.txt\n")
for line in inputfile:
    ll = line.split("\t")
    if(len(ll[1])>4):
        Czp_t[0].append(float(ll[0]))
        Czp_t[1].append(float(ll[1]))

inputfile.close()

#Czytanie pliku z poprawkami do wsp. momentu
xsak_tab = [[], [], [], [], []]#m, Cz, dXsak, xs/c, zs/c
inputfile = open("input/tabela.txt", "r")
print("Reading: input/tabela.txt\n")
for line in inputfile:
    ll = line.split("\t")
    if(len(ll[0])>1):
        for i, val in enumerate(ll):
            xsak_tab[i].append(float(val))

inputfile.close()

#Czytanie Cm płata
cmp_tab = [[], []]#alfa, cmp
inputfile = open("input/cmp.txt", "r")
print("Reading: input/cmp.txt\n")
for line in inputfile:
    ll = line.split("\t")
    if(len(ll[1])>4):
        cmp_tab[0].append(float(ll[0]))
        cmp_tab[1].append(float(ll[1]))

inputfile.close()

#Czytanie Cm0k
Cm0k = 0
inputfile = open("input/Cm0k.txt", "r")
line = inputfile.readline()
print("Reading: input/Cm0k.txt\n")
print(line)
Cm0k = float(line)

inputfile.close()

#Czytanie parametrów aerodynamicznych ogona
inputfile = open("input/aerodynamika_ogona.txt", 'r')
params = [[], []]#nazwa parametru, wartość parametru
print("Reading: input/aerodynamika_ogona.txt\n")
for line in inputfile:
    ll=line.split("\t")
    if ',' in ll[1]:
        w_str = list(ll[1])
        w_str[w_str.index(',')] = '.'#zamiana przecinka na kropkę
        ll[1] = "".join(w_str)
    params[0].append(ll[0])
    params[1].append(float(ll[1]))

inputfile.close()

k_zakl = params[1][params[0].index("k_zakl")]

#Czytanie plików mocy rozporządzalnych dla zadanej wysokości
#Czytanie adresów plików mocy rozporządzalnej
inputfile = open("input/moce.txt", "r")
breaker = True
print("Reading: input/moce.txt\n")

eng_coef = 0
if(params[1][params[0].index("eng_on")]>0):
    eng_coef=1

Nr_tab = []#[ns [rps], V, Cz, Nn, m_dot, Vh2/V2, T]

ns_max = params[1][params[0].index("ns_max")]

for line in inputfile:
    if "\n" in line:
        m_file = open(line[:-1], 'r')
    else:
        m_file = open(line, 'r')
    Nr_tab.append([[], [], [], [], [], [], []])
    print("Reading: "+line+"\n")
    for line2 in m_file:
        ll = line2.split("\t")
        if(breaker==False and len(ll)==2):
            breaker=True
        if(breaker==True and len(ll)==2 and float(ll[1])==params[1][params[0].index("h_przelotu")]):#założenie: wysokość przelotu istnieje w wysokościach mocy
            breaker=False
        if(breaker==False and len(ll)!=2 and float(ll[6])<=params[1][params[0].index("Cz_max")] and float(ll[4])<=params[1][params[0].index("V_max")]):
            Nr_tab[-1][0].append(float(ll[3])/60.0)#ns [rps]
            Nr_tab[-1][1].append(float(ll[4]))#V
            Nr_tab[-1][2].append(float(ll[6]))#Cz
            Nr_tab[-1][3].append(float(ll[7]))#Nn
            Nr_tab[-1][6].append(float(ll[10])*Nr_tab[-1][0][-1]**2/ns_max**2)#T #float(ll[10]) #Nr_tab[-1][3][-1]/Nr_tab[-1][1][-1]
            angle_v = math.atan(Nr_tab[-1][1][-1]/(0.75*math.pi*Nr_tab[-1][0][-1]*params[1][params[0].index("Ds")]))
            Nr_tab[-1][4].append(3*math.pi*params[1][params[0].index("Ds")]**3/16*math.tan(params[1][params[0].index("angle_s")]-angle_v)*params[1][params[0].index("rho")]*Nr_tab[-1][0][-1])#ilość powietrza przerzuconego przez śmigło przez sekundę do zachowania prędkości #params[1][params[0].index("m_dot")]*Nr_tab[-1][0][-1]
            if(eng_coef==1):
                Nr_tab[-1][5].append(1.07*(Nr_tab[-1][1][-1]+Nr_tab[-1][3][-1]/Nr_tab[-1][6][-1]*math.tanh(2*(Nr_tab[-1][2][-1]/params[1][params[0].index("Cz_max")])**2)**1.5)**2/Nr_tab[-1][1][-1]**2)#Vh2/V2 ##(Nr_tab[-1][1][-1]+Nr_tab[-1][6][-1]/Nr_tab[-1][4][-1])**2/Nr_tab[-1][1][-1]**2 #(Nr_tab[-1][1][-1]**2+Nr_tab[-1][3][-1]/Nr_tab[-1][4][-1])/Nr_tab[-1][1][-1]**2
            else:
                Nr_tab[-1][5].append(1.07)
            #lis = ""
            #for ore in range(len(Nr_tab[-1])):
            #    lis = lis+str(Nr_tab[-1][ore][-1])+"\t"
            #print(lis+"\n")

inputfile.close()

#aproksymacja ładowanych parametrów
CzCx_poly = []
CzCxfn = []
CzAlfa_poly = []
CzAlfafn = []
VhCz_poly = []
VhCzfn = []
CzpAlfa_poly = np.polyfit(Czp_t[0], Czp_t[1], 6)
CzpAlfafn = np.poly1d(CzpAlfa_poly)
CmAlfa_poly = np.polyfit(cmp_tab[0], cmp_tab[1], 7)
CmAlfafn = np.poly1d(CmAlfa_poly)
mdotCz_poly = []
mdotCzfn = []
TCz_poly = []
TCzfn = []
for Cz_t in Cz_tab:
    CzCx_poly.append(np.polyfit(Cz_t[2], Cz_t[1], 6))
    CzCxfn.append(np.poly1d(CzCx_poly[-1]))
    CzAlfa_poly.append(np.polyfit(Cz_t[0], Cz_t[2], 6))
    CzAlfafn.append(np.poly1d(CzAlfa_poly[-1]))
for Nr_ind in Nr_tab:
    TCz_poly.append(np.polyfit(Nr_ind[2], Nr_ind[6], 8))
    VhCz_poly.append(np.polyfit(Nr_ind[2], Nr_ind[5], 17))
    mdotCz_poly.append(np.polyfit(Nr_ind[2], Nr_ind[4], 16))
    TCzfn.append(np.poly1d(TCz_poly[-1]))
    VhCzfn.append(np.poly1d(VhCz_poly[-1]))
    mdotCzfn.append(np.poly1d(mdotCz_poly[-1]))

def n_bezp(V):
    if(V<=params[1][params[0].index("Va")]):
        return V*params[1][params[0].index("n1a")]+params[1][params[0].index("n1b")]
    return V*params[1][params[0].index("n2a")]+params[1][params[0].index("n2b")]

alfa_tab = np.linspace(-3.45, max(Cz_tab[0][0]), 100)#min(Cz_tab[0][0])
Cz_n_tab = np.linspace(min(Nr_tab[0][2]), max(Nr_tab[0][2]), 2000)

#Obliczanie Cmbu

Cmbu = []#[m, alpha, Cz, Cx, Cmp, Cmk, Cmbu, Cms]
Cmg = []#[alfa, Cz, Czustab, epsilon, alfa_u, Czuustat, dCzu, dalfau_komp, Cmuustat, CMG]
mil = []#[dDhdV, dDhdn, dFdV, dFdn]

cmbuf = open("output/cmbu_file.txt", "w")
cmgf = open("output/cmg_file.txt", "w")

Xn = []
dCmbhdCz = 0
S_h=params[1][params[0].index("S_h")]
S = params[1][params[0].index("S")]
a_tab = [params[1][params[0].index("a")], params[1][params[0].index("a1")], params[1][params[0].index("a2")], params[1][params[0].index("b1")], params[1][params[0].index("b2")]]
ca = params[1][params[0].index("ca")]
x_h = params[1][params[0].index("x_h")]
deda = params[1][params[0].index("deda")]
rho = params[1][params[0].index("rho")]
a1p = a_tab[1]*(1-a_tab[2]/a_tab[1]*a_tab[3]/a_tab[4])
S_hs = params[1][params[0].index("S_hs")]
c_hs = params[1][params[0].index("c_hs")]
f_g = params[1][params[0].index("f_g")]
V_L = params[1][params[0].index("V_L")]
h_p = params[1][params[0].index("h_przelotu")]
V_L_INDX = 0
V_L_PARAM = 0.05
cmbuf.write("m\t\\alpha\tC_z\tCx\tCm_p\tCm_k\tCm_{bu}\tCm_{sil}\n")
cmgf.write("\\alpha\tCz\tCz_{ustab}\t\\epsilon\t\\alpha_u\tCz_{uustat}\tdCz_u\t\\Delta\\alphau_{komp}\tCm_{uustat}\tCMG\n")
for i in range(len(xsak_tab[0])):
    Cmbu.append([[], [], [], [], [], [], [], []])
    Cmg.append([[], [], [], [], [], [], [], [], [], [], []])
    for INDX_A, alfa in enumerate(alfa_tab):
        Cmbu[-1][0].append(xsak_tab[0][i])
        Cmbu[-1][1].append(alfa*math.pi/180.0)
        #print(alfa)
        Cmbu[-1][2].append(CzAlfafn[i](alfa))
        Vinf = math.sqrt(2*xsak_tab[0][i]*9.81/(rho*S*Cmbu[-1][2][-1]))
        if(Vinf<=V_L+V_L_PARAM and Vinf>=V_L-V_L_PARAM):
            V_L_INDX = INDX_A
        Cmbu[-1][3].append(CzCxfn[i](Cmbu[-1][2][-1]))
        Cmbu[-1][4].append(CmAlfafn(alfa)+Cmbu[-1][2][-1]*xsak_tab[3][i]-(Cmbu[-1][3][-1]-alfa*math.pi/180.0*Cmbu[-1][2][-1])*xsak_tab[4][i])
        Cmbu[-1][5].append(Cm0k+Cmbu[-1][2][-1]*xsak_tab[2][i]*(-1))
        Cmbu[-1][7].append(TCzfn[i](Cmbu[-1][2][-1])*poz_sil[2][i]/(0.5*rho*Vinf**2*S*ca)*eng_coef)#Cm_silnika #(math.sqrt(VhCzfn[i](Cmbu[-1][2][-1]))-1)*mdotCzfn[i](Cmbu[-1][2][-1])*poz_sil[2][i]/(0.5*rho*S*Vinf*S*ca)
        Cmbu[-1][6].append(Cmbu[-1][4][-1]+Cmbu[-1][5][-1]-Cmbu[-1][7][-1])        
        Cmg[-1][0].append(alfa*math.pi/180.0)
        Cmg[-1][1].append(CzpAlfafn(alfa))
        Cmg[-1][2].append(Cmbu[-1][6][-1]*S*ca/(Sh*x_h))#Czu_stab
        Cmg[-1][3].append(deda*Cmg[-1][0][-1])#epsilon #2/math.pi/Lmbd*Cmg[-1][1][-1]*0.988
        Cmg[-1][4].append(Cmg[-1][0][-1]+Cmg[-1][3][-1]+k_zakl*math.pi/180.0)#alfa_u
        Cmg[-1][5].append(a_tab[1]*Cmg[-1][4][-1])#Czu_ustat
        Cmg[-1][6].append(Cmg[-1][5][-1]-Cmg[-1][2][-1])#dCzu
        Cmg[-1][7].append(-Cmg[-1][6][-1]/a_tab[1])#dalfa_u
        Cmg[-1][8].append(Cmg[-1][5][-1]*Sh*x_h/S/ca)#Cmu_ustat
        Cmg[-1][9].append(Cmbu[-1][6][-1]-Cmg[-1][8][-1])
        Cmg[-1][10].append(Vinf)
        for o in range(len(Cmg[-1])):
            if(o<len(Cmbu[-1])):
                cmbuf.write("{0:.4f}".format(Cmbu[-1][o][-1]))
            cmgf.write("{0:.4f}".format(Cmg[-1][o][-1]))
            if(o<len(Cmbu[-1])-1):
                cmbuf.write("\t")
            if(o==len(Cmbu[-1])-1):
                cmbuf.write("\n")
            if(o<len(Cmg[-1])-1):
                cmgf.write("\t")
            else:
                cmgf.write("\n")

cmbuf.close()
cmgf.close()

dCmbhdA = []#alfa, dCmbh/dalfa

for i in range(len(xsak_tab[0])):
    Xn.append([[], [], [], [], [], [], [], [], [], [], [], [], [], []])#Cz, dCmbhdCz, dCmsc, Xn, V, Xn', Cnmq, Q, Xm, Cnmqp, Xmp, alpha_h, delta_h, M(delta_h)
    mil.append([[], [], [], [], [], [], [], []])
    dCmbhdA.append([[], []])
    Cz_min_V = 2*xsak_tab[0][i]*9.81/(rho*S*params[1][params[0].index("V_max")]**2)
    print(Cz_min_V)
    for o in range(len(alfa_tab)):
        if(o==0):
            #pierwsza pochodna dCmbh/dCz
            dCmbhdCz=(-3)/(2*(Cmbu[i][2][1]-Cmbu[i][2][0]))*(Cmbu[i][6][0]+Cmbu[i][7][0])+2/(Cmbu[i][2][1]-Cmbu[i][2][0])*(Cmbu[i][6][1]+Cmbu[i][7][1])-1/(2*(Cmbu[i][2][1]-Cmbu[i][2][0]))*(Cmbu[i][6][2]+Cmbu[i][7][2])
        if(o==len(alfa_tab)-1):
            #ostatnia pochodna dCmbh/dCz
            dCmbhdCz=1/(2*(Cmbu[i][2][o]-Cmbu[i][2][o-1]))*(Cmbu[i][6][o-2]+Cmbu[i][7][o-2])-2/(Cmbu[i][2][o]-Cmbu[i][2][o-1])*(Cmbu[i][6][o-1]+Cmbu[i][7][o-1])+3/(2*(Cmbu[i][2][o]-Cmbu[i][2][o-1]))*(Cmbu[i][6][o]+Cmbu[i][7][o])
        if(o!=0 and o<len(alfa_tab)-1):
            #wszystkie środkowe pochodne dCmbh/dCz
            dCmbhdCz=(Cmbu[i][6][o+1]+Cmbu[i][7][o+1]-Cmbu[i][6][o-1]-Cmbu[i][7][o-1])/(Cmbu[i][2][o+1]-Cmbu[i][2][o-1])
        if(Cmbu[i][2][o]>=Cz_min_V):
            dCmbhdA[-1][0].append(Cmbu[i][1][o])
            #Xn[-1][4].append(params[1][params[0].index("V_max")])
            if(o!=0 and o<len(alfa_tab)-1):
                dCmbhdA[-1][1].append((Cmbu[i][6][o+1]-Cmbu[i][6][o-1])/(Cmbu[i][1][o+1]-Cmbu[i][1][o-1]))
            if(o==len(alfa_tab)-1):
                dCmbhdA[-1][1].append(1/(2*(Cmbu[i][1][o]-Cmbu[i][1][o-1]))*Cmbu[i][6][o-2]-2/(Cmbu[i][1][o]-Cmbu[i][1][o-1])*Cmbu[i][6][o-1]+3/(2*(Cmbu[i][1][o]-Cmbu[i][1][o-1]))*Cmbu[i][6][o])
            if(o==0):
                dCmbhdA[-1][1].append((-3)/(2*(Cmbu[i][1][1]-Cmbu[i][1][0]))*Cmbu[i][6][0]+2/(Cmbu[i][1][1]-Cmbu[i][1][0])*Cmbu[i][6][1]-1/(2*(Cmbu[i][1][1]-Cmbu[i][1][0]))*Cmbu[i][6][2])
            Xn[-1][0].append(Cmbu[i][2][o])#Cz
            Xn[-1][1].append(dCmbhdCz)#dCmbh/dCz
            Xn[-1][2].append(dCmbhdCz+xsak_tab[3][i]*(1+S_h/S*VhCzfn[i](Cmbu[i][2][o])*a_tab[1]/a_tab[0]*(1-deda))-S_h/S*x_h/ca*VhCzfn[i](Cmbu[i][2][o])*a_tab[1]/a_tab[0]*(1-deda))#dCmsc/dCz
            Xn[-1][3].append((-dCmbhdCz+S_h/S*x_h/ca*VhCzfn[i](Cmbu[i][2][o])*a_tab[1]/a_tab[0]*(1-deda))/(1+S_h/S*VhCzfn[i](Cmbu[i][2][o])*a_tab[1]/a_tab[0]*(1-deda)))#Xn
            Xn[-1][4].append(math.sqrt(2*xsak_tab[0][i]*9.81/(S*rho*Xn[-1][0][-1])))#V
            Xn[-1][5].append((-dCmbhdCz+S_h/S*x_h/ca*VhCzfn[i](Cmbu[i][2][o])*a1p/a_tab[0]*(1-deda))/(1+S_h/S*VhCzfn[i](Cmbu[i][2][o])*a1p/a_tab[0]*(1-deda)))#Xn'
            Xn[-1][6].append(-(x_h/ca-Xn[-1][3][-1])*S_h/S*VhCzfn[i](Xn[-1][0][-1])*a_tab[1])#Cnmq
            Vinf = math.sqrt(2*xsak_tab[0][i]*9.81/(rho*S*Xn[-1][0][-1]))#V
            Xn[-1][7].append(9.81*xsak_tab[0][i])#Q#9.81*(n_bezp(Vinf)-1)/Vinf
            Xn[-1][8].append(Xn[-1][3][-1]-rho*9.81*S*ca*Xn[-1][6][-1]/(2*Xn[-1][7][-1]))#Xm
            Xn[-1][9].append(-(x_h/ca-Xn[-1][5][-1])*S_h/S*VhCzfn[i](Xn[-1][0][-1])*a1p)#Cnmq'
            Xn[-1][10].append(Xn[-1][5][-1]-rho*9.81*S*ca*Xn[-1][9][-1]/(2*Xn[-1][7][-1]))#Xm'
            Xn[-1][11].append(Xn[-1][0][-1]/a_tab[0]*(1-deda)+k_zakl)#alpha_h
            Xn[-1][12].append((Xn[-1][0][-1]/(a_tab[0]*a_tab[2])*(1-deda)-Cmbu[i][6][o]*S/S_h/(x_h/ca-xsak_tab[3][i])/VhCzfn[i](Xn[-1][0][-1])/a_tab[2]+k_zakl*math.pi/180.0*a_tab[1]/a_tab[2])*180.0/math.pi)#delta_h
            Xn[-1][13].append(0.5*rho*Xn[-1][4][-1]**2*S_hs*c_hs*a_tab[4]*Xn[-1][12][-1]*math.pi/180)#M(delta_h)
            mil[-1][0].append(-4*xsak_tab[0][i]*9.81*ca/(rho*Vinf**3*S_h*x_h*a_tab[2])*(Xn[-1][3][-1]-xsak_tab[3][i]))#dDhdV
            mil[-1][1].append(-2*xsak_tab[0][i]*9.81*ca/(rho*Vinf**2*S_h*x_h*a_tab[2])*(Xn[-1][8][-1]-xsak_tab[3][i]))#dDhdn
            mil[-1][2].append(-2*xsak_tab[0][i]*9.81/Vinf*ca/x_h*S_hs/S_h*a_tab[4]/a_tab[2]*c_hs*f_g*(Xn[-1][5][-1]-xsak_tab[3][i]))#dFdV
            mil[-1][3].append(xsak_tab[0][i]*9.81*ca/x_h*S_hs/S_h*a_tab[4]/a_tab[2]*c_hs*f_g*(Xn[-1][10][-1]-xsak_tab[3][i]))#dFdn
            mil[-1][4].append(Xn[-1][3][-1]-xsak_tab[3][i])#h_N
            mil[-1][5].append(Xn[-1][5][-1]-xsak_tab[3][i])#h_N
            mil[-1][6].append(Xn[-1][8][-1]-xsak_tab[3][i])#h_N
            mil[-1][7].append(Xn[-1][10][-1]-xsak_tab[3][i])#h_N
        #else:
            #Xn[-1][4].append(math.sqrt(2*xsak_tab[0][i]*9.81/(S*rho*Xn[-1][0][-1])))
        #print(str(Xn[-1][0][-1])+"\t"+str(Xn[-1][4][-1])+"\n")

#kąt zaklinowania
alfa_zh = []
print("aktualny kąt zaklinowania: {0}".format(k_zakl))
print("Zalecane kąty zaklinowania:")
zh_file = open("output/alfazh.txt", 'w')
zh_file.write("$m [kg]\t\\alfa_{zh} [\\^\\circ]$\n")
for i in range(len(xsak_tab[0])):
    Cz_L = 2*xsak_tab[0][i]*9.81/(rho*S*V_L**2)
    alfa_zh.append((S/S_h/(VhCzfn[i](Cz_L))*Cmbu[i][6][V_L_INDX]/a_tab[1]/(x_h/ca-xsak_tab[3][i])-Cz_L/a_tab[0]/a_tab[1]*(1-deda))*180/math.pi)
    print(alfa_zh[-1])
    zh_file.write("{0}\t{1:.2f}\n".format(xsak_tab[0][i], alfa_zh[-1]))
zh_file.close()

#Wykresy

#Wykres porównania współczynników siły nośnej od kąta natarcia
plt.figure(figsize=(30/2.54, 17.5/2.54))
for i in range(len(CzAlfafn)):
    plt.plot(alfa_tab, CzAlfafn[i](alfa_tab), "-", label="m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    plt.plot(Cz_tab[i][0], Cz_tab[i][2], "--o", label="m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Porównanie aproksymacji $Cz(\\alpha)$ z przebiegiem dyskretnym")
plt.xlabel("$\\alpha [^\\circ]$")
plt.ylabel("Cz")
plt.grid(True)
plt.legend()
plt.savefig("output/aproksymacjaCzalfa.png")

#Wykres porównania współczynników Vh
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Cz_n_tab, VhCzfn[i](Cz_n_tab), "-", label="m="+str(xsak_tab[0][i])+"kg")
    plt.plot(Nr_tab[i][2], Nr_tab[i][5], ":", label="m="+str(xsak_tab[0][i])+"kg")
plt.title(r"Porównanie aproksymacji $\frac{V_H^2}{V^2}(Cz)$ z przebiegiem dyskretnym")
plt.xlabel("$Cz [-]$")
plt.ylabel(r"$\frac{V_H^2}{V^2}$")
plt.xlim(0, 1.5)
#plt.ylim(1.0, 1.5)
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/aproksymacjaVhCz.png")

#Wykres porównania mdot
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Cz_n_tab, mdotCzfn[i](Cz_n_tab), "-", label="m="+str(xsak_tab[0][i])+"kg")
    plt.plot(Nr_tab[i][2], Nr_tab[i][4], ":", label="m="+str(xsak_tab[0][i])+"kg")
plt.title(r"Porównanie aproksymacji $\dot{m}(Cz)$ z przebiegiem dyskretnym")
plt.xlabel("$Cz [-]$")
plt.ylabel(r"$\dot{m}$")
plt.xlim(0, 1.5)
#plt.ylim(1.0, 1.5)
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/aproksymacjamdotCz.png")

#Wykres porównania T
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Cz_n_tab, TCzfn[i](Cz_n_tab), "-", label="(aproksymacja) m="+str(xsak_tab[0][i])+"kg")
    plt.plot(Nr_tab[i][2], Nr_tab[i][6], ":", label="(dyskretne) m="+str(xsak_tab[0][i])+"kg")
plt.title(r"Porównanie aproksymacji $T(Cz)$ z przebiegiem dyskretnym")
plt.xlabel("$Cz [-]$")
plt.ylabel(r"$T [N]$")
plt.xlim(0, 1.5)
#plt.ylim(1.0, 1.5)
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/aproksymacjaTCz.png")

#Wykres dCmsc/dCz
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][0][1:], Xn[i][2][1:], "-", label="m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres pochodnej współczynnika momentu pochylającego samolotu względem Cz")
plt.xlabel("$Cz [-]$")
plt.ylabel(r"$\frac{\partial Cm_{SC}}{\partial Cz}$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/dCmsc.png")

#Wykres dCmbu/dCz
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(dCmbhdA[i][0], dCmbhdA[i][1], "-", label="m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres pochodnej współczynnika momentu samolotu bez usterzenia względem $\\alpha$")
plt.xlabel("$Cz [-]$")
plt.ylabel(r"$\frac{\partial Cm_{bu}}{\partial Cz}$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/dCmbua.png")

#Wykres dCmsc/dCz(V)
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][4][1:], Xn[i][2][1:], "-", label="m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres pochodnej współczynnika momentu samolotu względem Cz w zależności od prędkości poziomej")
plt.xlabel(r"$V [\frac{m}{s}]$")
plt.ylabel(r"$\frac{\partial Cm_{SC}}{\partial Cz}$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/dCmscV.png")

#Wykres wędrówki Xn
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][0][1:], Xn[i][3][1:], "-", label="(N)m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    #plt.plot(Xn[i][0][1:], Xn[i][5][1:], ":", label="(N')m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres wędrówki punktu neutralnego stateczności samolotu względem Cz")
plt.xlabel("$Cz [-]$")
plt.ylabel(r"$\frac{X_N}{C_a}$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/XnCz.png")

#Wykres wychylenia steru w zależności od prędkości
plt.clf()
for i in range(len(VhCzfn)):
    for o in range(len(Xn[i][4])):
        Xn[i][12][o]=Xn[i][12][o]*(-1)
    plt.plot(Xn[i][4][1:], Xn[i][12][1:], "-", label="m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    #plt.plot(Xn[i][0][1:], Xn[i][5][1:], ":", label="(N')m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres wychylenia steru w zależności od prędkości")
plt.xlabel(r"$V [\frac{m}{s}]$")
plt.ylabel(r"$\delta_h \ [^\circ]$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/delta_hV.png")

#zapisanie danych o mdelta do pliku
mdelta = open("output/mdelta.txt", 'w')
mdelta.write("$m[kg]$\t$V[\\frac\{m\}\{s\}]$\t$\\delta_h[\\^circ]$\t$M(\\delta_h)[Nm]$\n")
for i in range(len(xsak_tab[0])):
    mdelta.write("m\t{0}\n".format(xsak_tab[0][i]))
    for o in range(len(Xn[i][4])):
        mdelta.write("{0}\t{1:.2f}\t{2:3f}\t{3:3f}\n".format(xsak_tab[0][i], Xn[i][4][o], Xn[i][12][o], Xn[i][13][o]))
mdelta.close()

#Wykres momentu od wychylenia steru w zależności od prędkości
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][4][1:], Xn[i][13][1:], "-", label="m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    #plt.plot(Xn[i][0][1:], Xn[i][5][1:], ":", label="(N')m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres momentu od wychylenia steru w zależności od prędkości")
plt.xlabel(r"$V [\frac{m}{s}]$")
plt.ylabel(r"$M(\delta_h) \ [Nm]$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/Mdelta_hV.png")

#Wykres wędrówki Xn(V)
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][4][1:], Xn[i][3][1:], "-", label="(N) m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    #plt.plot(Xn[i][4][1:], Xn[i][5][1:], ":", label="(N') m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres wędrówki punktu neutralnego stateczności samolotu względem Cz")
plt.xlabel(r"$V [\frac{m}{s}]$")
plt.ylabel(r"$\frac{X_N}{C_a}$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/XnV.png")

#Wykres wędrówki Xm
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][0][1:], Xn[i][8][1:], "-", label="(M)m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    #plt.plot(Xn[i][0][1:], Xn[i][10][1:], ":", label="(M')m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres wędrówki punktu neutralnego sterowności samolotu względem Cz")
plt.xlabel("$Cz [-]$")
plt.ylabel(r"$\frac{X_M}{C_a}$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/XmCz.png")

#Wykres wędrówki Xm(V)
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][4][1:], Xn[i][8][1:], "-", label="(M) m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    #plt.plot(Xn[i][4][1:], Xn[i][10][1:], ":", label="(M') m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres wędrówki punktu neutralnego sterowności samolotu względem V")
plt.xlabel(r"$V [\frac{m}{s}]$")
plt.ylabel(r"$\frac{X_M}{C_a}$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/XmV.png")

#Wykres dDh(V)
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][4][1:], mil[i][0][1:], "-", label=r"$\frac{\partial \delta_H}{\partial V}$ m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    plt.plot(Xn[i][4][1:], mil[i][1][1:], ":", label=r"$\frac{\partial \delta_H}{\partial n}$ m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres pochodnych wychylenia steru po prędkości i współczynniku bezpieczeństwa")
plt.xlabel(r"$V [\frac{m}{s}]$")
plt.ylabel(r"$\frac{\partial \delta_H}{\partial V} \left [ \frac{rad}{\frac{m}{s}} \right ], \frac{\partial \delta_H}{\partial n} \left [ \frac{rad}{1} \right ]$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/dDh.png")

#Wykres dF(V)
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][4][1:], mil[i][2][1:], "-", label=r"$\frac{\partial F_D}{\partial V}$ m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    plt.plot(Xn[i][4][1:], mil[i][3][1:], ":", label=r"$\frac{\partial F_D}{\partial n}$ m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres pochodnych siły na sterze po prędkości i współczynniku bezpieczeństwa")
plt.xlabel(r"$V [\frac{m}{s}]$")
plt.ylabel(r"$\frac{\partial F}{\partial V} \left [ \frac{N}{\frac{m}{s}} \right ], \frac{\partial F}{\partial n} \left [ \frac{N}{1} \right ]$")
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/dF.png")

#Wykres zapasu hN(V)
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][4][1:], mil[i][4][1:], "-", label=r"$h_N$ m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    #plt.plot(Xn[i][4][1:], mil[i][5][1:], ":", label=r"$h_{N'}$ m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres zapasu stateczności")
plt.xlabel(r"$V [\frac{m}{s}]$")
plt.ylabel(r"$h_N \left [ \frac{m}{m} \right ]$")#, h_{N'} \left [ \frac{m}{m} \right ]
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/hN.png")

#Wykres zapasu h(V)
plt.clf()
for i in range(len(VhCzfn)):
    plt.plot(Xn[i][4][1:], mil[i][6][1:], "-", label=r"$h_M$ m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
    #plt.plot(Xn[i][4][1:], mil[i][7][1:], ":", label=r"$h_{M'}$ m="+str(xsak_tab[0][i])+"kg", color = colors[(i*3)%len(colors)])
plt.title("Wykres zapasu sterowności")
plt.xlabel(r"$V [\frac{m}{s}]$")
plt.ylabel(r"$h_M \left [ \frac{m}{m} \right ]$")#
plt.grid(True)
plt.legend()
#plt.show()
plt.savefig("output/hM.png")

#Wykres porównania Cmp(alfa)
plt.clf()
plt.plot(alfa_tab, CmAlfafn(alfa_tab), "-", label="Aproksymacja")
plt.plot(cmp_tab[0], cmp_tab[1], "--o", label="wartości dyskretne")
plt.title("Porównanie aproksymacji $Cm_p(\\alpha)$ z przebiegiem dyskretnym")
plt.xlabel("$\\alpha [^\\circ]$")
plt.ylabel("$Cm_p$")
plt.grid(True)
plt.legend()
plt.savefig("output/aproksymacjaCmpalfa.png")

#Wykres porównania Czp(alfa)
plt.clf()
plt.plot(alfa_tab, CzpAlfafn(alfa_tab), "-", label="Aproksymacja")
plt.plot(Czp_t[0], Czp_t[1], "--o", label="wartości dyskretne")
plt.title("Porównanie aproksymacji $Cm_p(\\alpha)$ z przebiegiem dyskretnym")
plt.xlabel("$\\alpha [^\\circ]$")
plt.ylabel("$Cz_p$")
plt.grid(True)
plt.legend()
plt.savefig("output/aproksymacjaCzpalfa.png")

#Wykres Cmbu
plt.clf()
for i in range(len(Cmbu)):
    plt.plot(Cmbu[i][2], Cmbu[i][6], label = "m="+str(Cmbu[i][0][0])+"kg", color = colors[(i*3)%len(colors)])
plt.legend()
plt.grid(True)
plt.title("Wykresy współczynników momentów pochylająych dla różnych typów lotu")
plt.xlabel(r"$Cz$")
plt.ylabel(r"$Cm_{bu}$")
plt.savefig("output/Cmbu.png")

#Wykres Cmg
plt.clf()
for i in range(len(Cmg)):
    plt.plot(Cmg[i][1], Cmg[i][9], label = "m="+str(Cmbu[i][0][0])+"kg", color = colors[(i*3)%len(colors)])
plt.legend()
plt.grid(True)
plt.title("Wykresy współczynników momentów pochylająych dla różnych typów lotu")
plt.xlabel(r"$Cz$")
plt.ylabel(r"$Cm_{g}$")
plt.savefig("output/Cmg.png")

#Wykres Cmg
plt.clf()
for i in range(len(Cmg)):
    plt.plot(Cmg[i][10], Cmg[i][9], label = "m="+str(Cmbu[i][0][0])+"kg", color = colors[(i*3)%len(colors)])
plt.legend()
plt.grid(True)
plt.title("Wykresy współczynników momentów pochylająych dla różnych typów lotu")
plt.xlabel(r"$V [\frac{m}{s}]$")
plt.ylabel(r"$Cm_{g}$")
plt.savefig("output/CmgV.png")
