# -*- coding: UTF-8 -*-
import math
import numpy as np
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os

from multiprocessing import Pool

mode = "manual"

#format pliku [J, Cn, eta]
file = open('input/JCn.txt', 'r')

#sprawdzenie, czy istnieje folder output
if(not os.path.exists('output')):
	try:
		print("Creating \"output\" folder.\n")
		os.mkdir("output")
	except:
		print("Warning! Could not find nor create folder \"output\"!\n")

exp = open('output/moce_rozporzadzalne.txt', 'w')
paleta = open("input/paleta.color", 'r')

paleta_line = paleta.readline()
colors = paleta_line.split(" ")
paleta.close()

rozdzielczosc = 20#co ile metrów mają być prowadzone obliczenia
wys = [0, 25000]
skip = 65

D=0.2032#średnica śmigła w m
m = 4.5#masa maksymalna samolotu w kg
g = 9.81# przyspieszenie ziemskie
S=0.3#pole powierzchni w m^2
Sf=0.1067
Cz_max = 1.4484#maksymalne Cz statku
H = np.linspace(wys[0], wys[1], int((wys[1]-wys[0])/rozdzielczosc+1))#generuje wysokości co 20m w przedziale 0-10000m
N_ns = 3.140407692/2#/5.497444
N = 600#Maksymalna długotrwała moc silnika W#810*0.75
N_idle = 2.3*14.8#Moc biegu jałowego
ns_max = 19380/60
Cn_gran_rho = N/((ns_max**3)*(D**5))

h_przel = 100#wysokość przelotowa [m]
U_bat =4*3.7#Napięcie akumulatorów
T_lnd = 31.25#ilość sekund podczas swobodnego opadania (na podstawie wcześniejszych danych dla m=4.5kg ze 100m)
P_e = 10.67


def rho(h):
    return 1.225*math.pow(1-h/44300, 4.256)

def cst(h):
    return N_ns/(rho(h)*D**5)

def w(dn):
    return dn/(m*g)

def gamma(w, V):
    em = w/V
    if(em>1):
        return math.asin(1)
    if(em<-1):
        return math.asin(-1)
    return math.asin(w/V)

def eta_sil(N):
    if(N/U_bat<0):
        return 0.0375*N/U_bat
    else:
        return 0.75

print (Cn_gran_rho/rho(100))

tab = [[],[],[],[]]
for line in file:
    ll = line.split("\t")
    if(len(ll)>1):
       for i in range(len(ll)):
           tab[i].append(float(ll[i]))
file.close()

for i in range(len(tab[2])):
    tab[2][i] = tab[2][i]*0.76/0.85

CnJ_poly = np.polyfit(tab[0], tab[1], 9)
CnJfn = np.poly1d(CnJ_poly)#funkcja aproksymacji Cn(J)

CtJ_poly = np.polyfit(tab[0], tab[3], 8)
CtJfn = np.poly1d(CtJ_poly)

etaJ_poly = np.polyfit(tab[0], tab[2],8)
etaJfn = np.poly1d(etaJ_poly)

J_tab = np.linspace(0.05, 1.0, int((wys[1]-wys[0])/rozdzielczosc+1))

#print(tab)

#ładowanie Cx(Cz)
Cza_tab = [[],[],[]]#alfa, Cx, Cz
if(mode=="manual"):
        Cza_file = open("../wing_characteristics/output/Czalfa_statku.txt","r")
        for line in Cza_file:
            ll = line.split("\t")
            if(len(ll[1])>6):
                for i in range(3):
                    Cza_tab[i].append(float(ll[i]))
        Cza_file.close()

if(mode=="xflr"):
        Cza_tab_run_saving = False
        Cza_file = open("input/WING.txt")
        for line in Cza_file:
                ll = line.split(" ")
                ll = [var for var in ll if var]
                print(ll)
                if(len(ll)>2):
                        print(Cza_tab_run_saving)
                        if(Cza_tab_run_saving == True):
                                Cza_tab[0].append(float(ll[0]))
                                Cza_tab[1].append(float(ll[5]))
                                Cza_tab[2].append(float(ll[2]))
                                print(ll)
                        if(ll[0]=="alpha"):
                                print("found alpha")
                                Cza_tab_run_saving = True
        Cza_file.close()

Cz_max = max(Cza_tab[2])

CzCx = np.polyfit(Cza_tab[2], Cza_tab[1], 6)
CzCxfn = np.poly1d(CzCx)#funkcja oporu w zależności od siły nośnej
Cz_tab = np.linspace(min(Cza_tab[2]), max(Cza_tab[2]), int((wys[1]-wys[0])/rozdzielczosc+1))

def ns_fn(n):
    ns_tab = []
    for i in range(n):
        ns_tab.append(math.sqrt(cst(h)/CnJfn(J_tab[i])))
    return ns_tab

def V_fn():
    V_tb = []
    for i in range(n):
        V_tb.append(J_tab[i]*ns_max*D)
    return V_tb

def Nr_fn(n):
    Nr_tb = []
    for i in range(n):
        Nr_tb.append(etaJfn(J_tab[i])*rho(h)*D**5*ns_max**3*CnJfn(J_tab[i]))
    return Nr_tb

def Cz_fn(n):
    Cz_tb = []
    for i in range(n):
        Cz_tb.append(2*m*g/(rho(h)*J_tab[i]*ns_max*D**2*S))
    return Cz_tb

def Nn_fn(n):
    Nn_tb = []
    for i in range(n):
        Nn_tb.append(0.5*rho(h)*Sf*CzCxfn(2*m*g/(rho(h)*J_tab[i]*ns_max*D**2*S))*J_tab[i]*ns_max*D**3)
    return Nn_tb

def w_fn(n):
    w_tb = []
    for i in range(n):
        w_tb.append(w(etaJfn(J_tab[i])*rho(h)*D**5*ns_max**3*CnJfn(J_tab[i])-0.5*rho(h)*Sf*CzCxfn(2*m*g/(rho(h)*J_tab[i]*ns_max*D**2*S))*J_tab[i]*ns_max*D**3))
    return w_tb

def gamma_fn(n):
    gamma_tb = []
    for i in range(n):
        gamma_tb.append(gamma(w(etaJfn(J_tab[i])*rho(h)*D**5*ns_max**3*CnJfn(J_tab[i])-0.5*rho(h)*Sf*CzCxfn(2*m*g/(rho(h)*J_tab[i]*ns_max*D**2*S))*J_tab[i]*ns_max*D**3), J_tab[i]*ns_max*D))

wn = []
t=[0]
V_tab = [[], [], [], [], [], []]#V_min, V_gamma, V_w, V_max, gamma_min, w_min
exp.write("J\tCn\teta\tns[RPM]\tV[m/s]\tNr[W]\tCz\tNn\tw\tgamma\tT\n")
print("tabela mocy")
for h in H:
    exp.write("h\t{0:.0f}\n".format(h))
    wn.append([[], [], [], [], [], [], [], []])#ns, V, Nr, Cz, Nn, w, gamma, Thr
    #print(h)
    for i in range(len(J_tab)-1):
       #if(CnJfn(J_tab[i])>=Cn_gran_rho/rho(h)):
            wn[-1][0].append(math.sqrt(cst(h)/CnJfn(J_tab[i])))#ns#
            wn[-1][1].append(J_tab[i]*ns_max*D)#J_tab[i]*wn[-1][0][i]*D#V
            wn[-1][2].append(etaJfn(J_tab[i])*rho(h)*D**5*ns_max**3*CnJfn(J_tab[i]))#N_ns*wn[-1][0][i]*tab[2][i]#Nr
            wn[-1][3].append(2*m*g/(rho(h)*wn[-1][1][-1]**2*S))#Cz
            wn[-1][4].append(0.5*rho(h)*S*CzCxfn(wn[-1][3][-1])*wn[-1][1][-1]**3)#Nn
            wn[-1][5].append(w(wn[-1][2][-1]-wn[-1][4][-1]))#w
            wn[-1][6].append(gamma(wn[-1][5][-1], wn[-1][1][-1]))#gamma
            wn[-1][7].append(CtJfn(J_tab[i])*rho(h)*ns_max**2*D**4)#Thr
            exp.write("{0:.3f}\t{1:.3f}\t{2:.3f}\t{3:.3f}\t{4:.3f}\t{5:.3f}\t{6:.3f}\t{7:.3f}\t{8:.3f}\t{9:.3f}\t{10:.3f}\n".format(J_tab[i], CnJfn(J_tab[i]), etaJfn(J_tab[i]), wn[-1][0][-1]*60, wn[-1][1][-1], wn[-1][2][-1], wn[-1][3][-1], wn[-1][4][-1], wn[-1][5][-1], wn[-1][6][-1], wn[-1][7][-1]))
    if(h%1000.0==0):
        print("{0:.1f}%".format(h/max(H)*100))
exp.close()

h05 = 0
V_min=0
V_max=0
print("Tabela prędkości")
for h_t in range(1, len(H)):
    #print(h_t/len(H))
    if(len(wn[h_t][5])>0):
        if(max(wn[h_t][5])>=0.0):
            t.append(t[-1]+0.5*(1/max(wn[h_t-1][5])+1/max(wn[h_t][5]))*rozdzielczosc)
            V_tab[4].append(-999)
            V_tab[5].append(-999)
            V_tab[0].append(-999)
            for i in range(len(wn[h_t][1])):
                if(wn[h_t][3][i]<=Cz_max and V_tab[0][-1]==-999):
                    V_tab[0][-1] = wn[h_t][1][i]*3.6
                if(wn[h_t][5][i]<0 and wn[h_t][5][i-1]>0):
                    V_tab[3].append(wn[h_t][1][i]*3.600)
                if(wn[h_t][5][i]>V_tab[4][-1] and wn[h_t][3][i]<=Cz_max):
                    V_tab[4][-1] = wn[h_t][5][i]
                    V_min = wn[h_t][1][i]*3.600
                if(wn[h_t][6][i]>V_tab[5][-1]*math.pi/180.0 and wn[h_t][3][i]<=Cz_max):
                    V_tab[5][-1] = wn[h_t][6][i]*180.0/math.pi
                    V_max = wn[h_t][1][i]*3.600
            V_tab[1].append(V_max)
            V_tab[2].append(V_min)
        if(max(wn[h_t][5])>=0.49 and max(wn[h_t][5])<=0.51):
            h05 = H[h_t]
            print(h05)

pprak = H[len(V_tab[3])-1]#pułap praktyczny
print(pprak)
def eta_vmin(h):
    return 1+h**2*((V_tab[2][-1]/V_tab[0][-1]-1)/pprak**2)

for i in range(len(V_tab[0])):
    V_tab[0][i] = V_tab[0][i]*eta_vmin((i+1)*rozdzielczosc)

#Zasięg i długotrwałość lotu
Q_start = N*t[int(h_przel/rozdzielczosc)]/eta_sil(N)
Q_ladow = N_idle/eta_sil(N_idle)*T_lnd
Q_bat = U_bat*12*3600*0.85#Ilość użytecznej energii w akumulatorze [V*(Ah)*s/h]

i_h = int(h_przel/rozdzielczosc)
zsi = [[], [], [], [], []]#T, L, V, T2, L2
Q_lt = Q_bat-Q_start-Q_ladow
Q_lt2 = Q_bat/2-Q_start-Q_ladow
zs_file = open("output/zasiegdlugotrwalosc.txt", "w")
zs_file.write("V[m/s]\tT[h]\tL[km]\tT_q2[h]\tL_q2[km]\n")
for i in range(len(wn[i_h][1])):
    if(wn[i_h][4][i]<N and wn[i_h][3][i]<Cz_max):
        zsi[0].append(Q_lt/(wn[i_h][4][i]/(etaJfn(wn[i_h][1][i]/(wn[i_h][0][i]*D)*eta_sil(1)))+P_e)/3600)#T [h]
        zsi[1].append(zsi[0][-1]*3600*wn[i_h][1][i]/1000)#L
        zsi[2].append(wn[i_h][1][i])
        zsi[3].append(Q_lt2/(wn[i_h][4][i]/(etaJfn(wn[i_h][1][i]/(wn[i_h][0][i]*D)*eta_sil(1)))+P_e)/3600)#T2[h]
        zsi[4].append(zsi[3][-1]*3600*wn[i_h][1][i]/1000)
        zs_file.write("{0:.2f}\t{1:.2f}\t{2:.1f}\t{3:.2f}\t{4:.2f}\n".format(zsi[2][-1], zsi[0][-1], zsi[1][-1], zsi[3][-1], zsi[4][-1]))
zs_file.close()

plt.figure(figsize=(30/2.54, 17.5/2.54))
for i in range(int(len(H)/skip)+1):
    if(i*skip>=len(H)):
        i=len(H)-1
    if(len(wn[i*skip][1])>0):
        plt.plot(wn[i*skip][1], wn[i*skip][2], label = "H="+str(H[i*skip])+"m", color = colors[(int(i))%len(colors)])
plt.plot([0, max(wn[0][1])], [N, N], '--', color = "black", label="Maksymalna ciągła moc")
plt.xlabel(r"$V \ \left[ \frac{m}{s} \right ]$")
plt.ylabel(r"$Nr \ \left[ W \right ]$")
plt.grid(True)
plt.legend()
plt.title("Wykres mocy rozporządzalnej w zależności od prędkości")
plt.savefig("output/moce_pelne.png")
plt.xlim(0, 60)
plt.ylim(0,1500)
plt.title("Wykres mocy rozporządzalnej w zależności od prędkości (zbliżenie)")
plt.savefig("output/zblizenie.png")

plt.clf()
plt.plot(Cza_tab[2], Cza_tab[1], '--o', label="Cx(Cz) rzeczywiste")
plt.plot(Cz_tab, CzCxfn(Cz_tab), '-', label="Cx(Cz) aproksymowane (7 st.)")
plt.grid(True)
plt.xlabel('Cz')
plt.ylabel('Cx')
plt.legend()
plt.title("Porównanie wyników aproksymacji z dyskretnymi, znanymi wartościami Cx(Cz)")
plt.savefig("output/porownanie_aproksymacjiCxCz.png")

#porównanie Cn(J) aproksymowanego i rzeczywistego
plt.clf()
plt.plot(tab[0], tab[1], '--o', label = "odczytane z wykresu")
plt.plot(J_tab, CnJfn(J_tab), label = "aproksymacja")
plt.xlabel("J")
plt.ylabel("Cn")
plt.title("Porównanie wyników aproksymacji z dyskretnymi, znanymi wartościami Cn(J)")
plt.legend()
plt.grid(True)
plt.savefig("output/aproksymacjaCnJ.png")

#ns(V, h)
plt.clf()
for i in range(int(len(H)/skip)+1):
    if(i*skip>=len(H)):
        i=len(H)-1
    if(len(wn[i*skip][1])>0):
        plt.plot(wn[i*skip][1], wn[i*skip][0], label = "H="+str(H[i*skip])+"m")
plt.title("Zależność obrotów od prędkości dla różnych wysokości")
plt.xlabel(r"V [\frac{m}{s}]")
plt.ylabel(r"ns [\frac{obr}{s}]")
plt.grid(True)
plt.legend()
plt.savefig("output/nsV.png")

#porównanie eta(J) aproksymowanego i rzeczywistego
plt.clf()
plt.plot(tab[0], tab[2], '--o', label = "odczytane z wykresu")
plt.plot(J_tab, etaJfn(J_tab), label = "aproksymacja")
plt.xlabel("J")
plt.ylabel(r"$\eta$")
plt.title("Porównanie wyników aproksymacji z dyskretnymi, znanymi wartościami $\\eta(J)$")
plt.legend()
plt.grid(True)
plt.savefig("output/aproksymacjaetaJ.png")

#wykresy wznoszenia
wgamma = []
for f, h in enumerate(H):
    wgamma.append([[], [], []])#V, w, gamma
    for i in range(len(wn[f][3])):
        if(len(wn[f][3])>0):
            if(wn[f][3][i]<=Cz_max/eta_vmin(h)**2 and max(wn[f][5])>(-0.1) and wn[f][4][i]<N and wn[f][5][i]>-1):#and tab[1][i]>=Cn_gran_rho/rho(h)
                wgamma[-1][0].append(wn[f][1][i])
                wgamma[-1][1].append(wn[f][5][i])
                wgamma[-1][2].append(wn[f][6][i]*180/math.pi)

plt.clf()
for en, i in enumerate(wgamma):
    if(en%(skip*2)==0 and len(i[0])>0):
        plt.plot(i[0], i[1], '-', label = "w, H="+str(H[en])+"m", color = colors[int(en/skip/2)%len(colors)])
        plt.plot(i[0], i[2], '--', label = r"$\gamma$, H="+str(H[en])+"m", color = colors[int(en/skip/2)%len(colors)])
print(len(V_tab[1]))
print(len(wn[:len(V_tab[1])][6][0]))
tb = [[], [], [], []]#V(max(w)), V(max(gamma)), w(V), gamma(V)
for i in range(len(V_tab[1])):
    #tb[0].append(wgamma[i][0][0])
    #tb[1].append(wgamma[i][1][0])
    #tb[2].append(wgamma[i][2][0])
    for j in range(len(wn[i][5])):
        if(wn[i][1][j]==V_tab[2][i]/3.600):
            tb[0].append(V_tab[2][i]/3.6)
            tb[2].append(wn[i][5][j])
        if(wn[i][1][j]==V_tab[1][i]/3.600):
            tb[1].append(V_tab[1][i]/3.6)
            tb[3].append(wn[i][6][j]*180.0/math.pi)
#print(wgamma)
plt.plot(tb[1], tb[3], ':', label = r"Linia $\max(\gamma(V))$", color = colors[(int(en/skip/2)+1)%len(colors)])
plt.plot(tb[0], tb[2], ':', label = r"Linia $\max(w(V))$", color = colors[(int(en/skip/2)+2)%len(colors)])
plt.grid(True)
plt.xlabel(r'V $\frac{m}{s}$')
plt.ylabel(r'$w \ [\frac{m}{s}], \gamma [^\circ]$')
#plt.ylim(-2, 13)
plt.legend(loc = "upper right")
plt.title("Wykres zależności prędkości pionowych i kątów opadania w zależności od prędkości poziomej")
plt.savefig("output/predkosci_pionowe.png")

#wykres ciągu
plt.clf()
for i in range(int(len(H)/skip)+1):
    if(i*skip>=len(H)):
        i=len(H)-1
    if(len(wn[i*skip][1])>0):
        plt.plot(wn[i*skip][1], wn[i*skip][7], label = "H="+str(H[i*skip])+"m")
#plt.plot([0, max(wn[0][1])], [N, N], '--', color = "black", label="Maksymalna ciągła moc")
plt.xlabel(r"$V \ \left[ \frac{m}{s} \right ]$")
plt.ylabel(r"$T \ \left[ N \right ]$")
plt.grid(True)
plt.legend()
plt.title("Wykres ciągu śmigła zależności od prędkości")
plt.savefig("output/ciagi_pelne.png")

#plik czasów wznoszenia
cdw_file = open("output/czasdowysokosc.txt", "w")
cdw_file.write("h[m]\tt[s]\n")
for i in range(len(t)):
    cdw_file.write("{0}\t{1:.1f}\n".format(H[i], t[i]))
cdw_file.close()

#wykres czasu wznoszenia
plt.clf()
plt.plot(H[0:len(t)], t)
plt.plot([h05, h05], [0, max(t)], '--k', label = "Pułap praktyczny")
plt.plot([100, 100], [0, max(t)], '--r', label = "Projektowa wysokość lotu")
plt.plot([0, H[len(t)]], [Q_bat/N, Q_bat/N], ':g', label = "Maksymalny czas lotu na pełnej mocy (12Ah)")
plt.plot([0, H[len(t)]], [Q_bat/N/2, Q_bat/N/2], ':', label = "Maksymalny czas lotu na pełnej mocy (6Ah)")
plt.title("Wykres przewidywanego czasu wznoszenia na zadaną wysokość")
plt.text(300, Q_bat/N+200, "{0:.0f}s".format(Q_bat/N))
plt.text(300, Q_bat/N/2+200, "{0:.0f}s".format(Q_bat/N/2))
plt.ylim(0, 10000)
plt.xlabel("Wysokość lotu [m]")
plt.ylabel("Czas wznoszenia [s]")
plt.grid(True)
plt.legend(loc="upper center")
plt.savefig("output/czasdowysokosc.png")
plt.ylim(0, 20)
plt.xlim(0, 130)
plt.legend(loc="upper left")
plt.savefig("output/czasdowysokosc_zblizenie.png")

#zasięg i długotrwałość lotu
plt.clf()
fig1 = plt.figure(figsize=(30/2.54, 17.5/2.54))
fig1.subplots_adjust(top=0.8)
bx1 = fig1.add_subplot(211)
bx2 = fig1.add_axes([0.125, 0.07, 0.775, 0.3])
bx1.plot(zsi[2], zsi[0], label = "Długotrwałość (12Ah)[s]")
bx1.plot(zsi[2], zsi[3], label = "Długotrwałość (6Ah)[s]")
bx2.plot(zsi[2], zsi[1], label = "Zasięg (12Ah) [km]")
bx2.plot(zsi[2], zsi[4], label = "Zasięg (6Ah) [km]")
bx1.legend()
bx2.legend()
bx1.set_title("Długotrwałość lotu w zależności od prędkości poziomej")
bx1.set_xlabel(r"V $\frac{m}{s}$")
bx1.set_ylabel(r"T [h]")
bx2.set_title("Zasięg lotu w zależności od prędkości poziomej")
bx2.set_xlabel(r"V $\frac{m}{s}$")
bx2.set_ylabel(r"L [km]")
bx1.grid(True)
bx2.grid(True)
plt.draw()
plt.savefig("output/zasiegdlugotrwalosc.png")

#charakterystyka wysokościowa
#plt.clf()
#plt.plot(V_tab[0][:len(V_tab[3])], H[:len(V_tab[3])], label = r"$V_{min}$")
#plt.plot(V_tab[3], H[:len(V_tab[3])], label = r"$V_{max}$")
#plt.plot(V_tab[2], H[:len(V_tab[1])], label = r"$V_{w}$")
#plt.plot(V_tab[1], H[:len(V_tab[3])], label = r"$V_{\gamma}$")
#plt.legend(loc = "lower left")
#plt.savefig("wykresofertowy.png")

plt.clf()
plt.figure(figsize=(30/2.54, 17.5/2.54))
host = host_subplot(111, axes_class=AA.Axes)
plt.title("Wykres ofertowy")
plt.subplots_adjust(bottom=0.25)
ax0 = host.twiny()
ax1 = host.twiny()
ax2 = host.twiny()
ax3 = host.twiny()

offset = -30
new_fixed_axis = ax0.get_grid_helper().new_fixed_axis
ax1.axis["right"] = new_fixed_axis(loc="bottom", axes = ax1, offset = (0, offset))
ax1.axis["right"].toggle(all=True)
ax2.axis["right"] = new_fixed_axis(loc="bottom", axes = ax2, offset = (0, 2*offset))
ax2.axis["right"].toggle(all=True)
ax3.axis["right"] = new_fixed_axis(loc="bottom", axes = ax3, offset = (0, 3*offset))
ax3.axis["right"].toggle(all=True)

#host.set_ylim(-10, 19200)
host.set_ylabel(r"h [m]")
host.set_xlabel(r"V $[\frac{km}{h}]$")

ax1.set_xlabel(r"w $[\frac{m}{s}]$")
ax2.set_xlabel(r"$\gamma [^{\circ}]$")
ax3.set_xlabel(r"$t_h [s]$")

host.plot(V_tab[0][:len(V_tab[3])], H[:len(V_tab[3])], label = r"$V_{min}$")
host.plot(V_tab[3], H[:len(V_tab[3])], label = r"$V_{max}$")
host.plot(V_tab[2], H[:len(V_tab[1])], label = r"$V_{w}$")
host.plot(V_tab[1], H[:len(V_tab[3])], label = r"$V_{\gamma}$")

host.plot([0, max(V_tab[3])], [H[len(V_tab[3])-1], H[len(V_tab[3])-1]], label = "Pułap teoretyczny")
host.plot([0, max(V_tab[3])], [h05, h05], label = "Pułap praktyczny")
print("Pułap teoretyczny:"+str(H[len(V_tab[3])-1]))
plp_file = open("output/pulap.txt", "w")
plp_file.write("Pułap teoretyczny:"+str(H[len(V_tab[3])-1]))
plp_file.write("\nPułap praktyczny:"+str(h05))
plp_file.close()

ax1.plot(V_tab[4], H[:len(V_tab[4])], label = r"Prędkość wznoszenia")
ax1.set_xlim(0, 25)
ax2.plot(V_tab[5], H[:len(V_tab[5])], label = r"Kąt wznoszenia $\gamma$")
ax2.set_xlim(0, 50)
ax3.plot(t, H[0:len(t)], label = r"Czas wznoszenia $[s]$")
ax3.set_xlim(0, 15000)

host.legend(bbox_to_anchor=(0.7,0.02), loc='lower left', borderaxespad=0.)
host.text(0, H[len(V_tab[3])-1]-500, "pułap teoretyczny: {0:.0f}m".format(H[len(V_tab[3])-1]))
host.text(0, h05-500, "pułap praktyczny: {0:.0f}m".format(h05))
plt.draw()
plt.savefig("output/wykresofertowy.png")
plt.title("Wykres ofertowy (zbliżenie)")
ax3.set_xlim(0, 100)
host.set_ylim(0, 130)
plt.draw()
plt.savefig("output/wykresofertowy_zblizenie.png")
