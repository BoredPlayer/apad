import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import math
import numpy as np
plt.rc('font', family='FreeMono')

#Czytanie parametrów geometrycznych
inputfile = open("../pitch_stability/input/aerodynamika_ogona.txt", 'r')
print("Reading: ../pitch_stability/input/aerodynamika_ogona.txt\n")
params = [[], []]#nazwa parametru, wartość parametru
print("Reading: input/aerodynamika_ogona.txt\n")
for line in inputfile:
    ll=line.split("\t")
    if ',' in ll[1]:
        w_str = list(ll[1])
        w_str[w_str.index(',')] = '.'#zamiana przecinka na kropkę
        ll[1] = "".join(w_str)
    params[0].append(ll[0])
    params[1].append(float(ll[1]))

inputfile.close()

#Czytanie rozkładu współczynnika momentu płata
inputfile = open("../pitch_stability/input/cmp.txt", "r")
print("Reading: ../pitch_stability/input/cmp.txt\n")
Cma_tab = [[], []]
for line in inputfile:
    ll = line.split("\t")
    if(len(ll[1])>3):
        Cma_tab[0].append(float(ll[0]))
        Cma_tab[1].append(float(ll[1]))

inputfile.close()

#Aproksymacja współczynnika momentu płata
Cma_poly = np.polyfit(Cma_tab[0], Cma_tab[1], 7)
Cmafn = np.poly1d(Cma_poly)
print(Cmafn(0))

#Czytanie pliku z poprawkami do wsp. momentu
xsak_tab = [[], [], [], [], []]#m, Cz, dXsak, xs/c, zs/c
inputfile = open("../pitch_stability/input/tabela.txt", "r")
print("Reading: ../pitch_stability/input/tabela.txt\n")
for line in inputfile:
    ll = line.split("\t")
    if(len(ll[0])>1):
        for i, val in enumerate(ll):
            xsak_tab[i].append(float(val))

inputfile.close()

b = params[1][params[0].index("b")]
S = params[1][params[0].index("S")]
rho_sk = params[1][params[0].index("rho_skrz")]
rho = params[1][params[0].index("rho")]
g=9.81
m=4.5
rho_bat = 2260*0.043*0.018
x_scp = 0.05
Cz_max = params[1][params[0].index("Cz_max")]
V = math.sqrt(2*m*g/(rho*S*Cz_max))
print(V)
Va = params[1][params[0].index("Va")]
alfa_nat=0
indeks_masowy = 3#xsak_tab[0].index(max(xsak_tab[0]))

c_r = params[1][params[0].index("c_r")]
c_t = params[1][params[0].index("c_t")]

def c(y):
    return y*(c_r-c_t)/(-b/2)+c_r

Cz_tab = [[], []]

Cz_file = open("../Analiza płata/4.5kg/shrenk.txt", 'r')
for line in Cz_file:
    ll = line.split("\t")
    if(len(ll[0])>=2):
        Cz_tab[0].append(float(ll[0]))
        Cz_tab[1].append(float(ll[3]))

Cz_file.close()

#inputfile = open("../Analiza płata/4.5kg/kappa.txt", "r")
#inputfile.close()

dy = b/2/len(Cz_tab[0])

#Obciążenie masowe skrzydła
def q(y):
    #if(y<0.06 or y>0.19):
    return rho_sk*1.4*10**(-3)*(y**2-4*y+4)*g
    #return rho_sk*1.4*10**(-3)*(y**2-4*y+4)*g+rho_bat*g

def n_bezp(V):
    #if(V<=params[1][params[0].index("Va")]):
    #    return V*params[1][params[0].index("n1a")]+params[1][params[0].index("n1b")]
    #return V*params[1][params[0].index("n2a")]+params[1][params[0].index("n2b")]
    return 6.945712605055847

P_tab = [[], [], [], [], [], [], []]#y, aero, q, p, dMaero, dMq, dM

for i in range(len(Cz_tab[1])-1):
    P_tab[0].append(Cz_tab[0][i])#y
    P_tab[1].append(0.5*rho*V**2*dy*(Cz_tab[1][i]*c(Cz_tab[0][i])+Cz_tab[1][i+1]*c(Cz_tab[0][i+1]))/2)#aero
    P_tab[2].append(-(q(Cz_tab[0][i])+q(Cz_tab[0][i+1]))/2*dy*n_bezp(Va))#q
    P_tab[3].append(P_tab[1][-1]+P_tab[2][-1])#aero-q
    P_tab[4].append(0.5*rho*V**2*c(P_tab[0][-1])**2*dy*Cmafn(alfa_nat)*n_bezp(Va))#dMaero
    P_tab[5].append((x_scp*P_tab[2][-1]))#dMq#xsak_tab[3][indeks_masowy]
    P_tab[6].append(P_tab[4][-1]+P_tab[5][-1])

#print(sum(P_tab[1]))

kappa = (m*g)/(2*sum(P_tab[1]))

for i in range(len(Cz_tab[1])-1):
    P_tab[1][i] = 0.5*rho*V**2*dy*(Cz_tab[1][i]*kappa*c(Cz_tab[0][i])+Cz_tab[1][i+1]*kappa*c(Cz_tab[0][i+1]))/2*n_bezp(Va)
    P_tab[3][i]=P_tab[1][i]+P_tab[2][i]
#print(sum(P_tab[1]))
#print(n_bezp(Va))
#print(sum(P_tab[2]))
#print(sum(P_tab[2])/n_bezp(Va))

T_tab = [[], [], []]#Siła tnąca, moment gnący, Moment skręcający

Tfile = open("sily.txt", "w")
print("Saving: sily.txt\n")
Tfile.write("y\tc\tL\tQ\tF\tMaero\tMq\tT\tMg\tMs\n[m]\t[m]\t[N]\t[N]\t[N]\t[Nm]\t[Nm]\t[N]\t[Nm]\t[Nm]\n")

for i in range(len(P_tab[0])):
    if(i==0):
        T_tab[0].append(P_tab[3][len(P_tab[0])-i-1])
        T_tab[1].append(0)
        T_tab[2].append(0)
    else:
        T_tab[0].append(P_tab[3][len(P_tab[0])-i-1]+T_tab[0][i-1])
        T_tab[1].append(T_tab[1][i-1]+T_tab[0][i-1]*dy)
        T_tab[2].append(T_tab[2][i-1]+P_tab[6][len(P_tab[0])-i-1])
T_tab[0].reverse()
T_tab[1].reverse()
T_tab[2].reverse()
for i in range(len(P_tab[0])):
    Tfile.write("{0:.3f}\t{9:.3f}\t{1:.3f}\t{2:.3f}\t{3:.3f}\t{6:.3f}\t{7:.3f}\t{4:.3f}\t{5:.3f}\t{8:.3f}\n".format(P_tab[0][i], P_tab[1][i], P_tab[2][i], P_tab[3][i], T_tab[0][i], T_tab[1][i], P_tab[4][i], P_tab[5][i], T_tab[2][i], c(P_tab[0][i])))
Tfile.close()

plt.figure(figsize=(30/2.54, 17.5/2.54))
plt.title("Wykres przebiegu jednostkowych sił na skrzydle")
plt.plot(P_tab[0], P_tab[1], label = "Siła aerodynamiczna")
plt.plot(P_tab[0], P_tab[2], label = "Siła masowa")
plt.plot(P_tab[0], P_tab[3], label = "Siła wypadkowa")
plt.plot(P_tab[0], P_tab[4], label = "Moment skręcający aero")
plt.plot(P_tab[0], P_tab[5], label = "Moment skręcający q")
plt.plot(P_tab[0], P_tab[6], label = "Moment skręcający")
plt.ylabel(r"Siła jednostkowa $\left [\frac{N}{m} \right]$, Moment jednostkowy $\left [\frac{Nm}{m} \right]$")
plt.xlabel("Odległość na skrzydle od osi kadłuba [m]")
plt.grid(True)
plt.legend()
plt.savefig("silyjednostkowe.png")
print("Saving: silyjednostkowe.png\n")

plt.clf()
plt.title("Wykres przebiegu sił wewnętrznych w skrzydle")
plt.plot(P_tab[0], T_tab[0], label = "Siła tnąca")
plt.plot(P_tab[0], T_tab[1], label = "Moment gnący")
plt.plot(P_tab[0], T_tab[2], label = "Moment skręcający")
plt.ylabel(r"Siła $\left [N \right]$, Moment $\left [Nm \right]$")
plt.xlabel("Odległość na skrzydle od osi kadłuba [m]")
plt.grid(True)
plt.legend()
plt.savefig("silywewnetrzne.png")
print("Saving: silywewnetrzne.png\n")
