import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import math
import numpy as np
plt.rc('font', family='FreeMono')

if(len(sys.argv)<6):
	print("Error: not enough parameters")
else:
	b=float(sys.argv[1])
	S = float(sys.argv[2])
	Lambda = b*b/S
	m = float(sys.argv[3])
	Pz_a = m*9.81#1.2255*S*47.22*47.22/2
	cr = float(sys.argv[4])
	ct = float(sys.argv[5])
	lmbd = ct/cr
	Cz_max = float(sys.argv[6])

	def ce(y):
		global S
		global b
		return 4*S/(math.pi*b)*math.sqrt(1-math.pow(2*y/b, 2))

	def ctf(y):
		global Pz_a
		global cr
		global ct
		global S
		global b
		lmbd = ct/cr
		return 2*S/(b*(1+lmbd))*(1-2*y/b*(1-lmbd))
		
	def Cz1(y):
		return (0.5*(1+ce(y)/ctf(y)))

	def Pz_(y):
		global b
		global S
		global ct
		global cr
		global Pz_a
		lmbd = ct/cr
		return 0.5*Pz_a*(4/(math.pi*b)*math.sqrt(1-(2*y/b)*(2*y/b))+2/(b*(1+lmbd))*(1-2*y/b*(1-lmbd)))

	y_tab = np.linspace(0, b/2, 200)
	pz_tab = []
	pz_el = []
	pz_kw = []
	c1_tab = []
	cz_p = []
	file = open("output/shrenk.txt", 'w')
	file.write("y\tPz\tCz1\tCz\n")
	index_max = 0
	ind = 0
	for i in y_tab:
		pz_cc = Pz_(i)
		pz_ce = Pz_a*4/(math.pi*b)*math.sqrt(1-(2*i/b)*(2*i/b))
		pz_ck = Pz_a*2/(b*(1+lmbd))*(1-2*i/b*(1-lmbd))
		pz_tab.append(pz_cc)
		pz_el.append(pz_ce)
		pz_kw.append(pz_ck)
		c1_tab.append(Cz1(i))
		if(c1_tab[-1]>c1_tab[index_max]):
			index_max = ind
		ind+=1
	kappa = float(Cz_max/c1_tab[index_max])
	for n, i in enumerate(y_tab):
		cz_p.append(Cz1(i)*kappa)
		file.write("{0:.3f}\t{1:.3f}\t{2:.3f}\t{3:.3f}\n".format(i, pz_cc, Cz1(i), cz_p[n]))
	kap = open("output/kappa.txt", "w")
	kap.write(str(kappa))
	kap.close()
	plt.figure(figsize = (25/2.54, 20/2.54))
	plt.subplot(111)
	plt.plot(y_tab, pz_tab, label="x")
	plt.plot(y_tab, pz_kw, label="trapezowy")
	plt.plot(y_tab, pz_el, label="eliptyczny")
	plt.title("Rozkład sił na skrzydle")
	plt.grid(True)
	plt.ylabel("Pz [N]")
	plt.xlabel("y [m]")
	plt.legend(loc = "lower left")
	plt.savefig("output/shrenk.png")

	plt.figure(figsize = (25/2.54, 20/2.54))
	plt.subplot(111)
	plt.plot(y_tab, c1_tab, label="$Cz_1$")
	plt.plot(y_tab, cz_p, label="Cz skalowane")
	plt.plot([0.65, 0.65],[min(c1_tab), max(cz_p)], '--k')
	plt.plot([0.9, 0.9],[min(c1_tab), max(cz_p)], '--k', label = "obszar działania lotek")
	plt.plot([0, b/2],[Cz_max, Cz_max], ':g', label = "$Cz_{max}$ skalowane")
	plt.title("Rozkład współczynnika siły nośnej na skrzydle")
	plt.grid(True)
	plt.ylabel(r"$Cz_1$")
	plt.xlabel("y")
	plt.legend(loc = "lower left")
	plt.savefig("output/Cz_jednostkowy.png")

	file.close()
