﻿# -*- coding: UTF-8 -*-
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import math
import numpy as np
plt.rc('font', family='FreeMono')

from sys import platform

#pobieranie danych z plików
#Cza_data = open("input/Czalfa.txt", 'r')

Cza_tab = [[], []]
Cza_tab_line = [[], []]
for line in open("input/Czalfa.txt", 'r', encoding = 'utf8'):
        Ll = line.split('\t', 1)
        Ll[1].replace('\n', '0')
        for i in range(0, 2):
                m_data = float(Ll[i])
                Cza_tab[i].append(m_data)

#ładowanie listy liniowej Cz
for i in range(0, len(Cza_tab[0])):
        if(True):#było: Cza_tab[0][i]<=max(Cza_tab[0])*0.9
                Cza_tab_line[0].append(Cza_tab[0][i])
                Cza_tab_line[1].append(Cza_tab[1][i])

#pobieranie danych z pliku
CxCz_tab = [[], []]
for line in open("input/CxCz.txt", 'r', encoding = 'utf8'):
        Ll = line.split('\t', 1)
        Ll[1].replace('\n', '0')
        for i in range(0, 2):
                m_data = float(Ll[i])
                CxCz_tab[i].append(m_data)

#dane wbudowane
skala_rysunku = 1./50. #skala rysunku
g = 9.81 #przyspieszenei ziemskie
ro0 = 101325/(287*(273.15+15)) #gęstość powietrza przy ziemi [kg/m³]
ni0 = 1.8*math.pow(10, -5) #lepkość powietrza dynamiczna
c0 = 0.2 #cięciwa na początku skrzydła [m]
ck = 0.1 #cięciwa na końcu skrzydła [m]
b = 2.0 #rozpiętość skrzydeł [m]
S = 0.5*(c0+ck)*b #powierzchnia skrzydeł [m²]
m = 4.5 #masa w locie [kg]
rho = 1.213

#wygląd wykresów

#sprawdzenie, czy istnieje folder output
if(not os.path.exists('output')):
	try:
		print("Creating \"output\" folder.\n")
		os.mkdir("output")
	except:
		print("Warning! Could not find nor create folder \"output\"!\n")

#obliczenia
lmbd = ck/c0 #zbieżność (λ)
Lambda = b*b/S #wydłużenie (Λ)
ca = 2*c0*(1+lmbd+lmbd*lmbd)/(3*(1+lmbd)) #średnia cięciwa aerodynamiczna [m]
Re0 = 20.0*ca/1.5*math.pow(10, 5)
vx0 = 0 #kąt skosu natarcia [°]
xn = b*math.tan(vx0*math.pi/180.0)*(1+2*lmbd)/(6*(1+lmbd))
Czmax = max(Cza_tab[1]) #współczynnik Cz
Vs1 = math.sqrt(2*m*g/(rho*S*Czmax))
Re1 = Vs1*ca/ni0
Cxmin2 = 0.0125 #minimalny współczynnik oporu dla Cz = 0, wzięty z wykresu
Cxmin1 = Cxmin2*math.pow((Re1/10000000), -0.11)

if platform == "linux" or platform == "linux2":
        os.system("python3 shrenk.py {0:.5f} {1:.5f} {2:.5f} {3:.5f} {4:.5f} {5:.5f}".format(b, S, m, c0, ck, Czmax))
if platform == "win32":
        os.system("%python% shrenk.py {0:.5f} {1:.5f} {2:.5f} {3:.5f} {4:.5f} {5:.5f}".format(b, S, m, c0, ck, Czmax))

Cz_liniowe = np.polyfit(Cza_tab_line[0], Cza_tab_line[1], 1) #szacowanie linii regresji
print(Cz_liniowe)
Cz_fn = np.poly1d(Cz_liniowe) #funkcja zgodna z szacowaniem linii regresji

#rysowanie linii regresji
plt.figure(figsize = (25*1.5/2.54, 20*1.5/2.54))
plt.title("Wpółczynnik siły nośnej przybliżony liniowo")
#plt.plot(Cza_tab_line[0], Cza_tab_line[1], c="red",marker="o", label = 'obszar analizowany')
plt.plot(Cza_tab[0], Cza_tab[1], 'yo', label = 'Cz odczytane z wykresu')#plt.plot(Cza_tab[0], Cza_tab[1], 'yo', label = 'Cz odczytane z wykresu')
plt.plot(Cza_tab_line[0], Cz_fn(Cza_tab_line[0]), 'b--', label = 'przybliżone Cz', linewidth=2)
plt.xlim(-10, 15)
plt.ylim(-0.3, 1.8)
plt.xlabel(u'α [°]')
plt.ylabel(u'Cz(α)')
plt.legend(bbox_to_anchor=(1, 0), loc='lower right', borderaxespad=0.)
plt.grid(True)
plt.savefig('output/Cz-lin.png')

#Modelowanie Cx wielomianem 5 stopnia
plt.clf()
plt.title("Współczynnik oporu profilu")
Cx_mod = np.polyfit(CxCz_tab[0], CxCz_tab[1], 8)
CxCz_tab_lin = np.linspace(min(CxCz_tab[0]), max(CxCz_tab[0]))
Cx_fn = np.poly1d(Cx_mod)
plt.plot(CxCz_tab[0], CxCz_tab[1], 'y--o', label = 'Cx(Cz) odczytane z wykresu')
plt.plot(CxCz_tab_lin, Cx_fn(CxCz_tab_lin), '--b', label = "Cx(Cz) modelowane wielomianem", linewidth=2)
plt.xlim(-0.25, 1.75)
plt.ylim(0.01, 0.03)
plt.xlabel(u'Cz')
plt.ylabel(u'Cx')
plt.legend(bbox_to_anchor=(0, 1), loc='upper left', borderaxespad=0.)
plt.grid(True)
plt.savefig('output/Cx-mod.png')

#obliczanie korekty Cx
def DeltaCxRe(Cz):
	#return (Cxmin2-Cxmin1)*(1-math.fabs(Cz/Czmax))
    return 0

#korekta współczynnika oporu profilu związana z liczbą Reynoldsa
def Cxprim(Cz):
        return Cx_fn(Cz)+DeltaCxRe(Cz)

a_inf = Cz_liniowe[0]*180.0/math.pi
beta25 = 0#0.25*math.atan((c0-ck)/(0.5*b))*180/math.pi
d_tab = [0.0537*Lambda/a_inf-0.005, -0.43*math.pow(lmbd, 5)+1.83*math.pow(lmbd, 4)-3.06*math.pow(lmbd, 3)+2.56*math.pow(lmbd, 2) - lmbd + 0.148, (-2.2*math.pow(10, -7)*math.pow(Lambda, 3)+math.pow(10, -7)*math.pow(Lambda, 2)+1.6*math.pow(10, -5))*math.pow(beta25, 3)+1]
delta = d_tab[0]*d_tab[1]*d_tab[2]/0.048
tau_tab = [0.023*math.pow(Lambda/a_inf, 3)-0.103*math.pow(Lambda/a_inf, 2)+0.25*Lambda/a_inf, -0.18*math.pow(lmbd, 5)+1.52*math.pow(lmbd, 4)-3.51*math.pow(lmbd, 3)+3.5*math.pow(lmbd, 2)-1.33*lmbd+0.17]
print(tau_tab)
tau = tau_tab[0]*tau_tab[1]/0.17
a_ap = a_inf/(1+a_inf/(math.pi*Lambda)*(1+tau))

def Cxi(Cz):
        return Cz*Cz/(math.pi*Lambda)*(1+delta)

#Zapis danych z tabeli 2.2
tabela22 = open("output/tabela 2.2.txt", 'w')
tabela22.write("Profil\n")
tabela22.write("L.p.\tCz\tCx\talfa\tdeltaCx\tCx'\n")
tabela22.write("L.p.\t[-]\t[-]\t[°]\t[-]\t[-]'\n")
saved_data = [[],[],[],[],[],[], [], [], [], [], []]#L.p., Cz, Cx, α, ΔCx, Cx', αi, αp, Cxi, Cxp, Cz∧
for i in range(len(Cza_tab[0])):
        saved_data[0].append(i)
        saved_data[1].append(Cza_tab[1][i])
        saved_data[2].append(Cx_fn(Cza_tab[1][i]))
        saved_data[3].append(Cza_tab[0][i])
        saved_data[4].append(0)#saved_data[4].append(DeltaCxRe(Cza_tab[1][i]))
        saved_data[5].append(Cxprim(Cza_tab[1][i]))
        if(saved_data[2][i]>0):
                tabela22.write("{0}\t{1:.4f}\t{2:.4f}\t{3:.1f}\t{4:.4f}\t{5:.4f}\n".format(i, Cza_tab[1][i], Cx_fn(Cza_tab[1][i]), Cza_tab[0][i], saved_data[4][i], Cxprim(Cza_tab[1][i])))

deltaCx_tech = 0.15*min(saved_data[5])
def Cxpprim(Cz):
        return Cxprim(Cz)+deltaCx_tech+Cxi(Cz)

def alfa_i(Cz):
        return Cz/(math.pi*Lambda)*(1+tau)

def alfa_p(alfa_inf, Cz):
        return alfa_inf*math.pi/180+alfa_i(Cz)

CzShrenk = []
kap = open("output/kappa.txt", "r")
kappa=float(kap.readline())
k2 = kappa/max(saved_data[1])
Czalpha_statku = open("output/Czalfa_statku.txt", "w")

tabela22.write("\nPłat\n")
tabela22.write("L.p.\talfa_i\talfa_p\tCz\tCx_i\tCx'_p\n")
tabela22.write("L.p.\t[°]\t[°]\t[-]\t[-]\t[-]\n")
Czalpha_statku.write("alpha\tCx\tCz\n[d]\t[-]\t[-]\n")
for Cz in saved_data[1]:
	CzShrenk.append(Cz*k2)
for i in range(len(Cza_tab[0])):
        saved_data[6].append(alfa_i(Cza_tab[1][i])*180/math.pi) #kąt indukowany
        saved_data[7].append(alfa_p(Cza_tab[0][i], Cza_tab[1][i])*180/math.pi) #kąt płata
        saved_data[8].append(Cxi(Cza_tab[1][i])) #Opór indukowany
        saved_data[9].append(Cxpprim(Cza_tab[1][i])) #Opór płata
        tabela22.write("{0}\t{1:.2f}\t{2:.2f}\t{5:.4f}\t{3:.4f}\t{4:.4f}\n".format(i, alfa_i(Cza_tab[1][i])*180/math.pi, alfa_p(Cza_tab[0][i], Cza_tab[1][i])*180/math.pi, Cxi(Cza_tab[1][i]), Cxpprim(Cza_tab[1][i]), CzShrenk[i]))
        Czalpha_statku.write("{0:.5f}\t{1:.5f}\t{2:.5f}\n".format(saved_data[7][-1], saved_data[9][-1], CzShrenk[i]))
a_l = np.polyfit(saved_data[7], CzShrenk, 1)

tabela22.close()
Czalpha_statku.close()

#rysowanie wykresu Cx(Cz) z poprawką
plt.clf()
plt.plot(CxCz_tab_lin, Cx_fn(CxCz_tab_lin), '--k', label = "Cx(Cz) modelowane wielomianem")
plt.plot(saved_data[1], saved_data[5], 'b-o', label = 'Współczynnik oporu płata')
plt.xlim(-1.0, 2.2)
plt.ylim(0, 0.05)
plt.xlabel(u'Cz')
plt.ylabel(u'Cx')
plt.grid(True)
plt.legend(bbox_to_anchor=(0, 1), loc='upper left', borderaxespad=0.)
plt.title("Współczynnik oporu profilu z poprawką")
plt.savefig('output/Cx-Cz.png')

#rysunek 2.2
plt.clf()
plt.figure(1, figsize=(10, 3))
plt.subplot(121)
plt.title(r"Porównanie charakterystyki $Cz(\alpha)$ płata i profilu")
plt.plot(Cza_tab[0], Cza_tab[1], 'b--o', label = 'Cz∞')
plt.plot(saved_data[7], CzShrenk, 'g-o', label = 'Cz∧')
#plt.plot([min(saved_data[7]), max(saved_data[7])], [kappa, kappa], 'r:', label=r"$Cz_{max}$")
plt.text(6, 1.25, r'$\infty$')
plt.text(11, 1.1, r'$\Lambda$')
plt.xlim(-8, 18)
plt.ylim(-0.5, 1.75)
plt.ylabel(u'$Cz[-]$')
plt.xlabel('$\\alpha [^\circ]$\n$\\alpha_p [^\circ]$')
plt.legend(bbox_to_anchor=(1, 0), loc='lower right', borderaxespad=0.)
plt.grid(True)
plt.subplot(122)
plt.title(r"Porównanie charakterystyki $Cx(Cz)$ płata i profilu")
plt.plot(CxCz_tab[1], CxCz_tab[0], 'b--o', label = 'Biegunowa profilu')
plt.plot(saved_data[9], CzShrenk, 'g-o', label = 'Biegunowa płata')
plt.text(0.01, 1.3, r'$\infty$')
plt.text(0.04, 0.75, r'$\Lambda$')
plt.ylim(-0.5, 1.75)
#plt.xlim(0, 0.1)
plt.xlabel(u'$Cx [-]$')
plt.legend(bbox_to_anchor=(1, 0), loc='lower right', borderaxespad=0.)
plt.grid(True)
plt.savefig('output/rys22.png')

#wykres aproksymacji
apro_txt = open("output/apro.txt", 'w')
apro_txt.write("alfa\t~Cz(alfa_p)\n")
apro_txt.write("[°]\t[-]\n")
apro = [[], [], [],[]]
for i in range(len(Cza_tab_line[0])):
        apro[0].append((alfa_p(Cza_tab_line[0][i], Cza_tab_line[1][i]))*180/math.pi)
        apro[1].append((apro[0][i]*math.pi/180.0*a_ap+Cz_liniowe[1])*k2)
        apro[2].append(Cza_tab_line[0][i]*Cz_liniowe[0]+Cz_liniowe[1])
        apro[3].append(CzShrenk[i])
        apro_txt.write("{0:.2f}\t{1:.4f}\n".format(apro[0][i], apro[1][i]))

apro_txt.close()

an_l = np.polyfit(apro[0], apro[3], 1)
an_l1 = np.polyfit(apro[0], apro[1], 1)
print(an_l)
print(an_l1)

plt.clf()
plt.plot(apro[0], apro[3], color = 'green', marker = 'o', label = 'Cz(αp) (ze Shrenka)')
#plt.plot(apro[0], apro[2], color = 'red', marker = 'o')
plt.plot(apro[0], apro[1], color = 'red', marker = 'o', label = 'Przybliżony Cz')
plt.xlim(-10, 17.5)
plt.ylim(-0.3, 1.8)
plt.xlabel(r'$\alpha$ [°]')
plt.ylabel(r'$Cz(\alpha)$')
plt.grid(True)
plt.legend(bbox_to_anchor=(1, 0), loc='lower right', borderaxespad=0.)
plt.text(0, 1, "$a_\\Lambda$={0:.5f}".format(an_l[0]*180.0/math.pi))#
plt.text(5, 0.5, r"$a_{obl}$="+"{0:.5f}".format(a_ap))
plt.title(u"Aproksymacja charakterystyki Cz(alpha) płata nośnego\n(zakres liniowy)")
plt.savefig('output/apro.png')

#zapis obliczonych danych
dat = open("output/wyniki.txt", 'w')

dat.write("g\trho0\tni0\tc0\tck\tb\tS\tm\tRe0\tCzmax\tCxmin2\tCxmin1\tbeta_25\n")
dat.write("[m/s2]\t[kg/m3]\t[Pa*s]\t[m]\t[m]\t[m]\t[m2]\t[kg]\t[-]\t[-]\t[-]\t[-]\t[°]\n")
dat.write("{1}\t{2:.4f}\t{9}\t{3:.3f}\t{4:.3f}\t{5}\t{6}\t{7}\t{12:4.0f}\t{8}\t{10:.4f}\t{11:.4f}\t{13}\n\n".format(skala_rysunku, g, ro0, c0, ck, b, S, m, Czmax, ni0, Cxmin2, Cxmin1, Re0, beta25))
dat.write("ca\txn\tvx0\tlambda\tLAMBDA\tVs1\tRe1\t\ta_inf\ta_ap\ta_l\tdelta\ttau\tkappa\n")
dat.write("[m]\t[m]\t[°]\t[-]\t[-]\t[m/s]\t[-]\t\t[1/rad]\t[1/rad]\t[1/rad]\t[-]\t[-]\t[-]\n")
dat.write("{0:.4f}\t{1:.4f}\t{6:.4f}\t{2:.4f}\t{3:.4f}\t{4:.4f}\t{5:.1f}\t{7:.2f}\t{9:.4f}\t{12:.4f}\t{8:.4f}\t{10:.4f}\t{11:.4f}\n".format(ca, xn, lmbd, Lambda, Vs1, Re1, vx0, a_inf, delta, a_ap, tau, k2, a_l[0]*180.0/math.pi))

dat.close()

#rozkład Shrenka
