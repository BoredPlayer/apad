# -*- coding: UTF-8 -*-
import math
import numpy as np
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
import os

inputfile = open("input/mass.txt", "r")

tab = [[], [], [], [], [], [], [], [], []]#nazwa, masa, ilość, wymiar x, wymiar y, wymiar z, x, y, z
tab_indx = []
t_file_activation = False

#sprawdzenie, czy istnieje folder output
if(not os.path.exists('output')):
	try:
		print("Creating \"output\" folder.\n")
		os.mkdir("output")
	except:
		print("Warning! Could not find nor create folder \"output\"!\n")


#czytanie pliku z masami i pozycjami
for line in inputfile:
    ll=line.split("\t")
    #print(ll)
    if(ll[0]=='Nzw'):
        tab_indx.append(ll.index('Nzw'))
        tab_indx.append(ll.index('Masa'))
        tab_indx.append(ll.index('Ilość'))
    if(ll[0]==''):
        tab_indx.append(ll.index('wx'))
        tab_indx.append(ll.index('wy'))
        tab_indx.append(ll.index('wz'))
        tab_indx.append(ll.index('x'))
        tab_indx.append(ll.index('y'))
        tab_indx.append(ll.index('z\n'))
    print(tab_indx)
    if(t_file_activation==True):
        tab[0].append(ll[0])
        for i in range(1, len(tab)):
            if ',' in ll[tab_indx[i]]:
                w_str = list(ll[tab_indx[i]])
                w_str[w_str.index(',')] = '.'#zamiana przecinka na kropkę
                ll[tab_indx[i]] = "".join(w_str)
            if(len(ll[tab_indx[i]])==0):
                ll[tab_indx[i]]='0.01'
            tab[i].append(float(ll[tab_indx[i]]))
    if(ll[0]=='-'):
        t_file_activation=True

#plt.figure(figsize=(30/2.54, 17.5/2.54))
fig, ax = plt.subplots(1, figsize=(17.5/2.54, 17.5/2.54))

boxes = []
for c in range(len(tab)):
    rect = Rectangle((tab[6][c]-tab[3][c]/2.0, tab[8][c]-tab[5][c]/2.0), tab[3][c], tab[5][c])
    boxes.append(rect)
    ax.text(tab[6][c], tab[8][c], tab[0][c], fontsize="small")
pc = PatchCollection(boxes, facecolor='g', alpha=0.5, edgecolor='None')
ax.set_ylim(-0.5, 0.5)
ax.add_collection(pc)
ax.set_title("Rozkład mas i wielkości (XZ)")
plt.savefig("output/mass-xz.png", dpi=300)
plt.clf()
fig, ax = plt.subplots(1, figsize=(17.5/2.54, 17.5/2.54))
boxes = []
for c in range(len(tab)):
    rect = Rectangle((tab[7][c]-tab[4][c]/2.0, tab[8][c]-tab[5][c]/2.0), tab[4][c], tab[5][c])
    boxes.append(rect)
    ax.text(tab[7][c], tab[8][c], tab[0][c], fontsize="small")
pc = PatchCollection(boxes, facecolor='g', alpha=0.5, edgecolor='None')
ax.set_ylim(-0.5, 0.5)
ax.add_collection(pc)
ax.set_title("Rozkład mas i wielkości (YZ)")
plt.savefig("output/mass-yz.png", dpi=300)
fig, ax = plt.subplots(1, figsize=(17.5/2.54, 17.5/2.54))
boxes = []
for c in range(len(tab)):
    rect = Rectangle((tab[6][c]-tab[3][c]/2.0, tab[7][c]-tab[4][c]/2.0), tab[3][c], tab[4][c])
    boxes.append(rect)
    ax.text(tab[6][c], tab[7][c], tab[0][c], fontsize="small")
pc = PatchCollection(boxes, facecolor='g', alpha=0.5, edgecolor='None')
ax.set_ylim(-0.5, 0.5)
ax.add_collection(pc)
ax.set_title("Rozkład mas i wielkości (XY)")
plt.savefig("output/mass-xy.png", dpi=300)
