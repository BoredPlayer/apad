# APAD

APAD (Airplane Performance and Aerodynamics Designer) is an offline tool helping airplane designers with typical performance and filght characteristics calculations.

For now, software requires Python 3 with Matplotlib and NumPy packages. For detailed instaltion guide go to [Matplotlib](https://matplotlib.org/3.1.1/users/installing.html) and [NumPy](https://numpy.org/install/) documentation.

Detailed software documentation is under cosntruction. Current version: 0.1


Linux
========================================
No changes are necessary in order to run programs.


Windows
========================================
On Windows it is required to have an Environment Variable pointing towards Your "python.exe". If the variable is called differently than "python", change `%python%` command in line 77 of "wing_characteristics\wing_characteristics.py" to Your variable's name.
